#!/usr/bin/python3

# Copyright 2019 Christoph Conrads
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import sys


if len(sys.argv) < 2:
    msg = 'No PRNG type given, try `awc`, `cawc`, `swb`, or `swc`'
    print(msg, file=sys.stderr)
    sys.exit(1)


kind = sys.argv[1]

if kind not in ['awc', 'cawc', 'swb', 'swc']:
    print('unknown PRNG type {:s}'.format(kind), file=sys.stderr)
    sys.exit(2)


compute_modulus = \
    (lambda b, r, s: b**r + b**s - 1) if kind == 'awc' else \
    (lambda b, r, s: b**r + b**s + 1) if kind == 'cawc' else \
    (lambda b, r, s: b**r - b**s - 1) if kind == 'swb' else \
    (lambda b, r, s: b**r - b**s + 1) if kind == 'swc' else \
    sys.exit(3)


for e in [8,16,24,32,64]:
    # this is not implemented
    if e in [24,48] and kind != 'swc':
        continue

    b = 2**e
    r_max = \
        30 if e <= 16 else \
        40 if e <= 32 else \
        50

    for r in range(3, r_max):
        s_max = r-1 if r < 10 else r-2 if r < 20 else r-3

        for s in range(2, s_max):
            assert r > s

            if r % s == 0:
                continue

            # these will show at least twice otherwise: for base b and base b^2
            if r % 2 == 0 and s % 2 == 0:
                continue

            m = compute_modulus(b, r, s)

            print('{:6s} {:2d} {:2d} {:2d} {:d}'.format(kind, e, r, s, m))
