// Copyright 2019 Christoph Conrads
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

#include <cassert>
#include <cstdio>
#define private public
#include <random>

// This code shows the n-th draw for a ranlux24 pseudo-random number generator
// with seeds from 1 to 100 after skipping a large number of initial values. If
// you plot drawn values over the seeds, patterns will emerge if the PRNG used
// to initialize the ranlux generator is similar in mathematical structure,
// e.g., a linear congruential generator.



int main()
{
	constexpr auto n1 = 11u;
	constexpr auto n2 = 17u;
	constexpr auto n3 = 20u;

	static_assert(n1 < n2, "");
	static_assert(n2 < n3, "");

	std::printf("%u %u %u\n", n1, n2, n3);

	for(auto i = 0u; i < 100u; ++i)
	{
		auto gen = std::ranlux24_base();

		std::fill_n(gen._M_x, 24u, 0u);

		for(auto j = 0u; j < 12u; ++j)
			gen._M_x[j] = i + 1u;
		
		gen.discard(1000u*1000u);

		gen.discard(n1-1u);
		auto x1 = gen();

		gen.discard(n2-n1-1u);
		auto x2 = gen();

		gen.discard(n3-n2-1u);
		auto x3 = gen();

		std::printf("%lu %lu %lu\n", x1, x2, x3);
	}
}
