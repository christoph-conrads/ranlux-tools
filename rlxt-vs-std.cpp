// Copyright 2019 Christoph Conrads
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

// RANLUX Tools versus C++ Standard Library
//
// The code in this file compares the running time and throughput of the
// ranlux-tools subtract-with-borrow implementation with the C++11 standard
// library class `std::subtract_with_carry_engine`.

#include "benchmark.hpp"
#include <cassert>
#include <cstdio>
#include <cstdint>
#include <ctime>
#include <limits>
#include <random>
#include <string>
#include <type_traits>


namespace rlxt = ranlux_tools;

using ranlux8_base =
	std::subtract_with_carry_engine<std::uint8_t, 8u, 3u, 7u>;
using ranlux16_base =
	std::subtract_with_carry_engine<std::uint16_t, 16u, 3u, 11u>;
using ranlux32_base =
	std::subtract_with_carry_engine<std::uint32_t, 32u, 3u, 17u>;
using ranlux64_base =
	std::subtract_with_carry_engine<std::uint64_t, 64u, 7u, 13u>;


using ranlux8_swb_base =
	rlxt::subtract_with_borrow_engine<std::uint8_t, 8u, 3u, 7u>;
using ranlux16_swb_base =
	rlxt::subtract_with_borrow_engine<std::uint16_t, 16u, 3u, 11u>;
using ranlux32_swb_base =
	rlxt::subtract_with_borrow_engine<std::uint32_t, 32u, 3u, 17u>;
using ranlux64_swb_base =
	rlxt::subtract_with_borrow_engine<std::uint64_t, 64u, 7u, 13u>;


// the c++11 standard library ranlux has only luxury level 3 meaning it discards
// considerably less values than theoretically required
using ranlux24 =
	std::discard_block_engine<std::ranlux24_base, 389u, 24u>;


int main()
{
	constexpr auto num_draws = std::uintmax_t{1000} * 1000u * 1000u;

	std::printf(
		"%-25s | %10s | %20s | %s\n",
		"generator", "time(sec)", "throughput(byte/sec)", "dummy"
	);

	rlxt::run<rlxt::dummy_engine>(num_draws);
	rlxt::run<ranlux8_base>(num_draws);
	rlxt::run<ranlux8_swb_base>(num_draws);
	rlxt::run<ranlux16_base>(num_draws);
	rlxt::run<ranlux16_swb_base>(num_draws);
	rlxt::run<ranlux32_base>(num_draws);
	rlxt::run<ranlux32_swb_base>(num_draws);
	rlxt::run<ranlux64_base>(num_draws);
	rlxt::run<ranlux64_swb_base>(num_draws);
}
