#!/usr/bin/python3

# Copyright 2019 Christoph Conrads
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# This program reads data created by `ranlux-over-seed.cpp` from standard input
# and plots it.

import matplotlib.pyplot as pp
import numpy as np
import sys



def main():
    xs = np.loadtxt(sys.stdin, dtype=int)
    
    if xs.shape[1] != 3:
        return 1

    ns = xs[0,:]
    xs = xs[1:,:]

    pp.plot(xs[:,0], 'b.', linestyle="None", label='n={:d}'.format(ns[0]))
    pp.plot(xs[:,1], 'g+', linestyle="None", label='n={:d}'.format(ns[1]))
    pp.plot(xs[:,2], 'rx', linestyle="None", label='n={:d}'.format(ns[2]))

    pp.ylim(top=1.025 * 2**24);
    pp.ylim(bottom=-0.025 * 2**24);

    pp.xlabel('Seed');
    pp.ylabel('std::ranlux24_base output');
    pp.legend()

    title = '' if len(sys.argv) < 2 else sys.argv[1]

    if title != '':
        pp.title(title)

    if len(sys.argv) > 2:
        filename = sys.argv[2]

        print('Saving plot to "{:s}"'.format(filename))
        pp.savefig(filename)
    else:
        pp.show()


if __name__ == '__main__':
    main()
