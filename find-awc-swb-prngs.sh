#!/bin/sh

# Copyright 2019 Christoph Conrads
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

set -e
set -u


is_modulus_prime()
{
	local type="${1:?}"
	local w="${2:?}"
	local r="${3:?}"
	local s="${4:?}"

	if [ "$type" = awc ]
	then
		local modulus="$(python -c "b = 2**$w; m=b**$r+b**$s-1; print(m)")"
	elif [ "$type" = swc ]
	then
		local modulus="$(python -c "b = 2**$w; m=b**$r-b**$s+1; print(m)")"
	elif [ "$type" = swb ]
	then
		local modulus="$(python -c "b = 2**$w; m=b**$r-b**$s-1; print(m)")"
	else
		>&2 echo BUG is_modulus_prime
		exit 1
	fi

	openssl prime "$modulus" | fgrep --quiet 'is prime'
}

configure_cxx_code()
{
	local type="${1:?}"
	local w="${2:?}"
	local r="${3:?}"
	local s="${4:?}"

	sed -e "s/@WORD_SIZE@/$w/" \
		-e "s/@LONG_LAG@/$r/" \
		-e "s/@SHORT_LAG@/$s/" \
		"../compute-luscher-trajectory-$type.cpp.in"
}


make_luscher_plot()
{
	local type="${1:?}"
	local w="${2:?}"
	local r="${3:?}"
	local s="${4:?}"
	local cxx_filename="compute-luscher-trajectory-$type-$w-$r-$s.cpp"
	local binary="compute-luscher-trajectory-$type-$w-$r-$s"

	if ! $(is_modulus_prime $type $w $r $s)
	then
		>&2 echo "modulus of $type w=$w r=$r s=$s is not prime"
		return
	fi

	configure_cxx_code $type $w $r $s >"$cxx_filename"
	c++ -Wextra -Wall -std=c++11 -pedantic "$cxx_filename" -o "$binary"

	local stats="$("./$binary" | \
		python3 "../plot-luscher-trajectory.py" $type $w $r $s)"

	echo -n "$stats" ' '

	local period="$( \
		python3 "$cwd/compute-awc-swb-period-length.py" $type $w $r $s)"

	echo "$period"
}



find_prng_parameters()
{
	local type="${1:?}"
	local candidates_file="$type-candidates.txt"
	local results_file="$type-primality-checks.txt"

	python3 "$cwd/generate-awc-swb-moduli.py" "$type" >"$candidates_file"

	cat "$candidates_file" \
		| awk '{print $5}' \
		| xargs -n 1 openssl prime >"$results_file"

	paste "$candidates_file" "$results_file" \
		| fgrep 'is prime' \
		| awk '{printf("%6s %2d %2d %2d\n", $1, $2, $3, $4)}'
}



find_awc_swb_prngs()
{
	type="${1:?}"
	echo "Searching for $type parameter combinations with prime modulus..."

	local generators="$(find_prng_parameters "$type")"
	local num_generators="$(wc -l <<<"$generators")"

	if [ -z "$generators" ]
	then
		echo "Found no $type parameter combinations"
		return
	fi

	echo "Found $num_generators $type parameter combinations"

	echo 'type  b  r  s    t    p     rr  log2period log10period'
	while read gen
	do
		local b="$(awk '{print $2}' <<<"$gen")"
		local r="$(awk '{print $3}' <<<"$gen")"
		local s="$(awk '{print $4}' <<<"$gen")"

		make_luscher_plot "$type" $b $r $s
	done <<<"$generators"
}


cwd="$(pwd)"
mkdir -p -- "luscher-trajectory-plots"
cd -- "luscher-trajectory-plots"


# set the python matplotlib back-end
export MPLBACKEND="cairo"

find_awc_swb_prngs awc
find_awc_swb_prngs cawc
find_awc_swb_prngs swb
find_awc_swb_prngs swc
