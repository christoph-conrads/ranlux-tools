# Copyright 2019 Christoph Conrads
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

CXX := g++
CXXFLAGS := -O3 -march=native -DNDEBUG

SOURCES := \
	awc-vs-swb.cpp \
	batch-vs-sequential.cpp \
	benchmark.cpp \
	conditional-vs-promotion.cpp \
	prng-optimization.cpp \
	rlxt-vs-std.cpp print-random.cpp \
	swc24-vs-native-ints.cpp
EXECUTABLES := $(patsubst %.cpp,%.bin,$(SOURCES))

.PHONY: run

all: $(EXECUTABLES)


%.bin: %.cpp benchmark.hpp random-number-engine.hpp
	$(CXX) -Wextra -Wall -std=c++11 -pedantic \
		$(CXXFLAGS) \
		$< -o $@
