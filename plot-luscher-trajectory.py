#!/usr/bin/python3

# Copyright 2019 Christoph Conrads
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import matplotlib.pyplot as pp
import numpy as np
import scipy.optimize
import sympy.ntheory
import sys

if len(sys.argv) < 5:
	msg = '5 command line arguments needed, {:d} given'.format(len(sys.argv))
	print(msg, file=sys.stderr)
	sys.exit(1)

kind = sys.argv[1]
w = int(sys.argv[2])
r = int(sys.argv[3])
s = int(sys.argv[4])
filename = "luscher-trajectory-{:s}-{:d}-{:d}-{:d}.png".format(kind, w, r, s)

if w < 8 or r <= s or s < 2:
	print('Error: w={:d} r={:d} s={:d}'.format(w, r, s), file=sys.stderr)
	sys.exit(1)

data = np.loadtxt(sys.stdin)
xs = data[:,0]
ys = data[:,1]

if len(xs.shape) != 1:
	sys.exit(1)

if min(ys) < 0 or max(ys) >= 1:
	print('Error: min={:e} max={:e}'.format(min(ys),max(ys)), file=sys.stderr)
	sys.exit(1)


# fit function
nnz, = np.nonzero(ys > 2**-3)
k0 = None if nnz.size == 0 else min(nnz)

b = 2**w
t = (xs > 0) & (xs > 0 if k0 is None else xs <= k0)

assert np.any(t)

result = scipy.optimize.lsq_linear(np.expand_dims(xs[t]-1,1), np.log(1.0*b*ys[t]))
c = np.asscalar(result.x)

# The constant below is the expected distance of uniformly distributed points in
# a hypercube with word size b, cf. Lüscher (1993), Section 4.1.
# This expression is hidden behind the magic value  12/25 on p. 9.
y_min = (b/2) / (b+1)
# Compute the smallest x rounded up such that 1/b * e^(c(x-1)) >= y_min
k = int(np.ceil(np.log(y_min*b) / c + 1))
p = sympy.ntheory.nextprime(k*r)
rr = r / p


text_fmt = '{:4s} {:2d} {:2d} {:2d}  {:3d} {:4d}  {:5.3f}'
print(text_fmt.format(kind, w, r, s, k, p, rr))

zs = np.exp(c * (xs-1)) / b

pp.plot(xs, ys, 'b.', markersize=10, label='Measurement')
pp.plot(xs, zs, 'g', label='1/b exp({:.2f} (t-1))'.format(c))
pp.grid(which="both")

pp.ylim(top=1.0)
pp.ylim(bottom=2.0**-(w+1))
pp.yscale('log', basey=2)

pp.legend(loc='lower right')
pp.xlabel("t")
pp.ylabel("d(x(t),y(t))")
if kind == 'swc':
	pp.title("{:s}(2^{:d}, {:d}, {:d})".format('SWB', w, s, r))
else:
	pp.title("{:s}(2^{:d}, {:d}, {:d})".format(kind.upper(), w, r, s))

pp.savefig(filename)
