// Copyright 2019 Christoph Conrads
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

#include "benchmark.hpp"
#include <cassert>
#include <cstdio>
#include <cstdint>
#include <ctime>
#include <limits>
#include <random>
#include <string>
#include <type_traits>


namespace rlxt = ranlux_tools;

using swc8 =
	std::subtract_with_carry_engine<std::uint8_t,   8, 10, 24>;
using swc16 =
	std::subtract_with_carry_engine<std::uint16_t, 16, 10, 24>;
using swc24 = std::ranlux24_base;
using swc32 =
	std::subtract_with_carry_engine<std::uint32_t, 32, 10, 24>;
using swc64 =
	std::subtract_with_carry_engine<std::uint64_t, 64, 10, 24>;



int main()
{
	constexpr auto num_draws = std::uintmax_t{1000} * 1000u * 1000u;

	std::printf(
		"%-25s | %10s | %20s | %s\n",
		"generator", "time(sec)", "throughput(byte/sec)", "dummy"
	);

	rlxt::run<rlxt::dummy_engine>(num_draws);
	rlxt::run<swc8>(num_draws);
	rlxt::run<swc16>(num_draws);
	rlxt::run<swc24>(num_draws);
	rlxt::run<swc32>(num_draws);
	rlxt::run<swc64>(num_draws);
}
