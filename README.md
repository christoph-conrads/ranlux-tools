# RANLUX Tools

ranlux pseudo-random number generators (PRNGs) are a family of high-quality PRNGs with long periods. This git repository contains programs searching for valid ranlux parameters. The code is licensed under the [Mozilla Public License 2.0](https://www.mozilla.org/en-US/MPL/2.0/).

This code requires
* Bash,
* a C++11 compiler,
* a Python3 interpreter,
* Matplotlib, NumPy, and SymPy for Python3.

A ranlux PRNG works as follows:
* It draws `p` random values from an add-with-carry (AWC) PRNG or a subtract-with-borrow (SWB) PRNG.
* It returns `r <= p` values and discards the rest.
* Repeat.

Ranlux PRNGs have four parameters and all of them are positive integers:
* Base `b >= 2`, long lag `r`, and short lag `s` with `r > s`.
* Block size `p >= r`.

Valid parameter choices must fulfill certain number theoretical criteria and the search for such parameters is automated by the scripts in this repository.

AWC and SWB PRNGs were introduced in a paper by Marsaglia and Zaman:

> G. Marsaglia, A. Zaman: "A New Class of Random Number Generators." In: The Annals of Applied Probability, Vol. 1.3 (1991), pp. 462-480.

AWC and SWB PRNGs have solid theoretical foundations but they fail empirical random number generator tests and lead to inaccurate results in physics simulations:

> B. Hayes: "The Wheel of Fortune." In: American Scientist, Vol. 81.2 (1993), pp. 114-118. https://core.ac.uk/display/21804963

Judicously discarding values resolves these issues and this was discussed by Lüscher:

> M. Lüscher: "A Portable High-Quality Random Number Generator for Lattice Field Theory Simulations." In: Computer Physics Communications, Vol. 79.1 (1994), pp. 100-110. arXiv:hep-lat/9309020.

The Bash script `find-awc-swb-prngs.sh` finds AWC/SWB parameters and block sizes. The output has to be read as follows:
* `awc` denotes with an add-with-carry PRNG with base `b`, long lag `r`, and short lag `s`.
* `cawc` is a complementary AWC generator.
* `swb` is a subtract-with-borrow PRNG with base `b`, long lag `r`, and short lag `s`. Its recurrence is `x_n = x_{n-r} - x_{n-s} - c`, where `c` is the carry bit.
* `swc` is a subtract-with-borrow PRNG with recurrence `x_n = x_{n-s} - x_{n-r} - c` (note the different order of the terms). This generator is also an SWB but seeing that the C++11 standard library calls the class implementing this generator `subtract_with_carry`, I decided to use the acronym `swc`.
* `t` is the time parameter from Lüscher's chaotic dynamical system discussion of AWC and SWBs PRNGs.
* `log2period` and `log10period` show the base-2 and base-10 logarithm of the period length bounds.

Computing the period length requires decomposing large integers into their prime factors. This is a hard problem and in many cases I did not even attempt to factorize numbers. In fact, the prime factorizations hard-coded in `compute-awc-swb-period-length.py` were mostly computed by [GMP-ECM](http://ecm.gforge.inria.fr/) and some by [Cado-NFS](http://cado-nfs.gforge.inria.fr/). To factorize numbers, find small factors first with GMP-ECM using the procedure outlined in the section "How to use P-1, P+1, and ECM efficiently?" of the GMP-ECM README, run Cado-NFS afterwards. The idea here is that the run-time of ECM depends on the number of digits of a desired prime factor whereas the run-time of Cado-NFS is a function of the size of the number. I strongly suggest to always enable primality testing when running GMP-ECM or you may waste hours of computing time (`ecm -primetest ...`).

To compute the block size `p`, it is suggested to use the smallest prime number larger than `t * r`, e.g., for the popular 24-bit ranlux, we have `b=2^24`, `r=24`, `s=10`, and `t = 16`. It holds that `t * p = 16 * 24 = 384` so `p = 389`. Note that in practice, prime values larger than `t*p / 4` are often sufficient to pass all empirical random number generator tests.
