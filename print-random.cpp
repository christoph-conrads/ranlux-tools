// Copyright 2019 Christoph Conrads
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

// This program copies values drawn from the new RANLUX variants to the standard
// output file descriptor. This data can then be piped into other program, e.g.,
// dieharder:
// ./print-random | dieharder -a -g 200 -Y 1 -k 2

#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <limits>
#include "random-number-engine.hpp"
#include <type_traits>
#include <unistd.h>
#include <vector>



template<typename Generator>
void print()
{
	using T = typename Generator::result_type;

	static_assert(std::is_integral<T>::value, "");
	static_assert(not std::is_signed<T>::value, "");

	static_assert(std::numeric_limits<T>::max() == Generator::max(), "");
	static_assert(std::numeric_limits<T>::min() == Generator::min(), "");

	constexpr auto N = (std::size_t{1} << 20) / sizeof(T);
	auto gen = Generator();
	auto xs = std::vector<T>(N);

	while(true)
	{
		for(auto& x : xs)
			x = gen();

		constexpr auto num_bytes = N * sizeof(T);
		auto ret = write(STDOUT_FILENO, xs.data(), num_bytes);

		if(ret < 0)
		{
			std::fprintf(stderr, "write (ret=%zd)", ret);
			std::exit(1);
		}

		auto retu = std::size_t(ret);

		if(retu != num_bytes)
		{
			std::fprintf(
				stderr, "write (ret=%zu, num-bytes=%zu)", retu, num_bytes
			);
			std::exit(1);
		}
	}
}



#define COMPARE_AND_CALL(PRNG) \
	else if(generator == #PRNG) { print<rlxt::PRNG>(); }


int main(int argc, const char** argv)
{
	auto generator = std::string(argc < 2 ? "fast_ranlux32" : argv[1]);

	namespace rlxt = ranlux_tools;

	std::fprintf(stderr, "generator %s\n", generator.c_str());

	if(false) {}
	COMPARE_AND_CALL(ranlux8_base)
	COMPARE_AND_CALL(ranlux8)
	COMPARE_AND_CALL(fast_ranlux8)
	COMPARE_AND_CALL(ranlux16_base)
	COMPARE_AND_CALL(fast_ranlux16)
	COMPARE_AND_CALL(ranlux16)
	COMPARE_AND_CALL(ranlux32_base)
	COMPARE_AND_CALL(fast_ranlux32)
	COMPARE_AND_CALL(ranlux32)
	COMPARE_AND_CALL(ranlux64_base)
	COMPARE_AND_CALL(fast_ranlux64)
	COMPARE_AND_CALL(ranlux64)
}
