// Copyright 2019 Christoph Conrads
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

#ifndef RADEMACHER_FPL_RANDOM_NUMBER_ENGINE_HPP
#define RADEMACHER_FPL_RANDOM_NUMBER_ENGINE_HPP

#include <cassert>
#include <cstddef>
#include <cstdint>
#include <limits>
#include <random>
#include <cstring>
#include <type_traits>

#if defined(__SIZEOF_INT128__) && __SIZEOF_INT128__ == 16
#define RANLUX_TOOLS_HAS_INT128 1
#else
#define RANLUX_TOOLS_HAS_INT128 0
#endif


namespace ranlux_tools {
namespace impl_random_number_engine
{
	constexpr std::uint32_t rotl(std::uint32_t x, unsigned k)
	{
		return (x << k) | (x >> (32 - k));
	}


	// minimize the amount of code needed to switch between AWC/SWB
	// implementations by providing a dummy integer to be used whenever int128
	// is unavailable.
	// this type allows us to use a simple if statement to switch the
	// implementation and the optimizer should take care of the rest.
	struct dummy_integer_t
	{
		dummy_integer_t(std::uintmax_t) {}

		operator std::uintmax_t() const { return 0; }
	};

	dummy_integer_t operator+ (dummy_integer_t, dummy_integer_t) { return 0; }
	dummy_integer_t operator+ (dummy_integer_t, std::uintmax_t) { return 0; }
	dummy_integer_t operator- (dummy_integer_t, dummy_integer_t) { return 0; }
	dummy_integer_t operator- (dummy_integer_t, std::uintmax_t) { return 0; }
	dummy_integer_t operator>> (dummy_integer_t, std::size_t) { return 0;}


#if RANLUX_TOOLS_HAS_INT128
#if __GNUC__
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpedantic"
#endif
	template<std::size_t w>
	using big_integer_t = typename std::conditional<
		w == 8u,  std::uint16_t, typename std::conditional<
		w == 16u, std::uint32_t, typename std::conditional<
		w == 32u, std::uint64_t, typename std::conditional<
		w == 64u, unsigned __int128, dummy_integer_t
	>::type>::type>::type>::type;
#if __GNUC__
#pragma GCC diagnostic pop
#endif
#else
	template<std::size_t w>
	using big_integer_t = typename std::conditional<
		w == 8u,  std::uint16_t, typename std::conditional<
		w == 16u, std::uint32_t, typename std::conditional<
		w == 32u, std::uint64_t, dummy_integer_t
	>::type>::type>::type;
#endif

}

/**
 * This class implements a xoshiro128+ pseudo-random number generator.
 *
 * D. Blackman, S. Vigna: "Scrambled Linear Pseudorandom Number Generators."
 * 2018. arXiv:1805.01407v1
 *
 * See Table 4 of the paper for TestU01 BigCrush results (column "S" shows
 * "systematic" failures and column "R" contains "repeated" failures"). The
 * public domain code for xoshiro128plus by Blackman and Vigna can be downloaded
 * from the xoshiro/xoroshiro website:
 *
 *   http://xoshiro.di.unimi.it
 */
struct xoshiro128plus
{
	using result_type = std::uint32_t;

	static constexpr result_type max() {
		return std::numeric_limits<result_type>::max();
	}
	static constexpr result_type min() {
		return std::numeric_limits<result_type>::min();
	}


	explicit xoshiro128plus(std::uint64_t seed=0u) :
		// initialize state with some ones here to avoid LFSR zeroland
		state_{
			0, ~std::uint32_t{0}, std::uint32_t(seed>>32), std::uint32_t(seed)
		}
	{
		// escape from zeroland, cf. Section 9 in Blackman/Vigna (2018)
		// this generator is fast so generously discard a few values
		discard(128);
	}


	std::uint32_t operator() ()
	{
		using impl_random_number_engine::rotl;

		auto retval = state_[0] + state_[3];
		auto t = state_[1] << 9;

		state_[2] ^= state_[0];
		state_[3] ^= state_[1];
		state_[1] ^= state_[2];
		state_[0] ^= state_[3];
		state_[2] ^= t;
		state_[3] = rotl(state_[3], 11);

		return retval;
	}


	void discard(unsigned long long n)
	{
		for(auto i = 0ull; i < n; ++i)
			(*this)();
	}


	std::uint32_t state_[4];
};



/**
 * This class implements an add-with-carry (AWC) pseudo-random number generator.
 *
 * This implementation supports only the word size 32 with 32-bit unsigned
 * integer types. This generator is initialized with the aid of a xoshiro128+
 * PRNG to avoid correlated outputs for AWC PRNGs with similar seeds.
 *
 * References:
 * * G. Marsaglia, A. Zaman: "A New Class of Random Number Generators." In: The
 *   Annals of Applied Probability, Vol. 1, No. 3, pp. 462--469. 1991.
 * * M. Matsumoto et al.: "Defects in Initialization of Pseudorandom Number
 *   Generators." In: ACM Transactions on Modeling and Computer Simulation,
 *   Vol. 17, No. 4, Article 15. 2007. DOI: 10.1145/1276927.1276928
 */
template<typename T, std::size_t w, std::size_t s, std::size_t r>
struct add_with_carry_engine
{
	static_assert(std::is_integral<T>::value, "");
	static_assert(not std::is_signed<T>::value, "");
	static_assert(w <= std::numeric_limits<T>::digits, "");
	static_assert(s > 0u, "");
	static_assert(r > s, "");

	// prototype implementation
	static_assert(w == std::numeric_limits<T>::digits, "");
	static_assert(w == 8u or w == 16u or w == 32u or w == 64u, "");

	using result_type = T;

	static constexpr auto long_lag = r;
	static constexpr auto short_lag = s;
	static constexpr auto word_size = w;
	// `std::subtract_with_carry_engine::default_seed` is 19780503. I have no
	// idea where this value is coming from so I use its larger prime factor
	// instead.
	static constexpr auto default_seed = std::uint32_t{387853};


	static constexpr T max() { return std::numeric_limits<T>::max(); }
	static constexpr T min() { return std::numeric_limits<T>::min(); }


	explicit add_with_carry_engine(std::uint64_t seed=default_seed)
	{
		auto gen = xoshiro128plus(seed);

		for(auto& x : xs_)
			x = gen();

		// ensure entering a periodic sequence
		discard(r);
	}


	T operator() ()
	{
		using BigInt = impl_random_number_engine::big_integer_t<w>;
		using DummyInt = impl_random_number_engine::dummy_integer_t;

		auto i = index_;
		auto j = index_ >= s ? index_ - s : index_ + r - s;

		assert(carry_ == 0 or carry_ == 1);

		if(std::is_same<BigInt, DummyInt>::value)
		{
			auto x = xs_[i];
			auto y = T(x + xs_[j]);
			auto z = T(y + carry_);

			xs_[i] = z;
			carry_ = y < x or z < y ? 1u : 0u;
		}
		else
		{
			auto x = BigInt{xs_[i]} + xs_[j] + carry_;

			xs_[i] = x;
			carry_ = x >> w;
		}

		index_ = long_lag == 8 or long_lag == 16 or long_lag == 32
			? (index_ + 1u) & (long_lag-1u)
			: index_ + 1u == long_lag ? 0u : index_ + 1u
		;

		return xs_[i];
	}



	void discard(unsigned long long n)
	{
		for(auto i = 0ull; i < n; ++i)
			(*this)();
	}


	std::size_t index_ = 0;
	T carry_ = 0;
	T xs_[r] = { 0 };
};



template<typename T, std::size_t w, std::size_t p, std::size_t q>
struct subtract_with_borrow_engine
{

	static constexpr auto word_size = w;
	static constexpr auto long_lag = p > q ? p : q;
	static constexpr auto short_lag = p > q ? q : p;

	static constexpr auto r = long_lag;
	static constexpr auto s = short_lag;

	static_assert(std::is_integral<T>::value, "");
	static_assert(not std::is_signed<T>::value, "");
	static_assert(w <= std::numeric_limits<T>::digits, "");
	static_assert(short_lag > 0u, "");
	static_assert(long_lag > short_lag, "");

	// prototype implementation
	static_assert(w == std::numeric_limits<T>::digits, "");
	static_assert(w == 8u or w == 16u or w == 32u or w == 64u, "");

	using result_type = T;

	// `std::subtract_with_carry_engine::default_seed` is 19780503. I have no
	// idea where this value is coming from so I use its larger prime factor
	// instead.
	static constexpr auto default_seed = std::uint32_t{387853};


	static constexpr T max() { return std::numeric_limits<T>::max(); }
	static constexpr T min() { return std::numeric_limits<T>::min(); }


	explicit subtract_with_borrow_engine(std::uint64_t seed=default_seed)
	{
		auto gen = xoshiro128plus(seed);

		for(auto& x : xs_)
			x = gen();

		// ensure entering a periodic sequence
		discard(r);
	}


	T operator() ()
	{
		using BigInt = impl_random_number_engine::big_integer_t<w>;
		using DummyInt = impl_random_number_engine::dummy_integer_t;

		auto i = index_;
		auto j = index_ >= s ? index_ - s : index_ + r - s;

		assert(carry_ == 0 or carry_ == 1);

		if(std::is_same<BigInt, DummyInt>::value)
		{
			auto x = p > q ? xs_[i] : xs_[j];
			auto y = p > q ? T(x - xs_[j]) : T(x - xs_[i]);
			auto z = T(y - carry_);

			xs_[i] = z;
			carry_ = y > x or z > y ? 1u : 0u;
		}
		else
		{
			auto x = p > q
				? BigInt{xs_[i]} - xs_[j] - carry_
				: BigInt{xs_[j]} - xs_[i] - carry_
			;
			auto c = -T(x >> w);

			xs_[i] = x;
			carry_ = c;
		}

		index_ = long_lag == 8 or long_lag == 16 or long_lag == 32
			? (index_ + 1u) & (long_lag-1u)
			: index_ + 1u == long_lag ? 0u : index_ + 1u
		;

		return xs_[i];
	}


	void discard(unsigned long long n)
	{
		for(auto i = 0ull; i < n; ++i)
			(*this)();
	}


	std::size_t index_ = 0;
	T carry_ = 0;
	T xs_[long_lag] = { 0 };
};



using ranlux8_base = subtract_with_borrow_engine<std::uint8_t, 8u, 5u, 8u>;
using ranlux8 =  std::discard_block_engine<ranlux8_base, 67u, 8u>;
using fast_ranlux8  = std::discard_block_engine<ranlux8_base, 17u, 8u>;

using ranlux16_base = add_with_carry_engine<std::uint16_t, 16u, 2u, 9u>;
using ranlux16 = std::discard_block_engine<ranlux16_base, 97u, 9u>;
using fast_ranlux16 = std::discard_block_engine<ranlux16_base, 23u, 9u>;

using ranlux32_base = add_with_carry_engine<std::uint32_t, 32u, 3u, 16u>;
using ranlux32 = std::discard_block_engine<ranlux32_base, 389u, 16u>;
using fast_ranlux32 = std::discard_block_engine<ranlux32_base, 71u, 16u>;

using ranlux64_base = subtract_with_borrow_engine<std::uint64_t, 64u, 62u, 3u>;
using ranlux64 = std::discard_block_engine<ranlux64_base, 1303u, 62u>;
using fast_ranlux64 = std::discard_block_engine<ranlux64_base, 331u, 62u>;

}

#endif
