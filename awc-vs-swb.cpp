// Copyright 2019 Christoph Conrads
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

#include "benchmark.hpp"
#include <cassert>
#include <cstdio>
#include <cstdint>
#include <ctime>
#include <limits>
#include <random>
#include <string>
#include <type_traits>


namespace rlxt = ranlux_tools;


using awc16_base = rlxt::add_with_carry_engine<std::uint16_t, 16, 2, 9>;
using awc16 = std::discard_block_engine<awc16_base, 97, 9>;
using fast_awc16 = std::discard_block_engine<awc16_base, 23, 9>;

using swb16_base =
	rlxt::subtract_with_borrow_engine<std::uint16_t, 16, 3, 11>;
using swb16 = std::discard_block_engine<swb16_base, 127, 11>;
using fast_swb16 = std::discard_block_engine<swb16_base, 37, 11>;

using awc32_base = rlxt::add_with_carry_engine<std::uint32_t, 32, 3, 16>;
using awc32 = std::discard_block_engine<awc32_base, 277, 16>;
using fast_awc32 = std::discard_block_engine<awc32_base, 71, 16>;

using swb32_base =
	rlxt::subtract_with_borrow_engine<std::uint32_t, 32, 3, 17>;
using swb32 = std::discard_block_engine<swb32_base, 293, 17>;
using fast_swb32 = std::discard_block_engine<swb32_base, 73, 17>;

using swb64_base = rlxt::subtract_with_borrow_engine<std::uint64_t, 64, 62, 3>;
using swb64 = std::discard_block_engine<swb64_base, 1303, 62>;
using fast_swb64 = std::discard_block_engine<swb64_base, 331, 62>;


// performance depends unfortunately on long lag and short lag
using swx64_base =
	rlxt::subtract_with_borrow_engine<std::uint64_t, 64, 15, 2>;





int main()
{
	constexpr auto num_draws = std::uintmax_t{1000} * 1000u * 1000u;

	std::printf(
		"%-26s | %10s | %20s | %s\n",
		"generator", "time(sec)", "throughput(byte/sec)", "dummy"
	);

	rlxt::run<rlxt::dummy_engine>(num_draws);

	std::printf("\n");

	rlxt::run<awc16_base>(num_draws);
	rlxt::run<swb16_base>(num_draws);
	rlxt::run<awc32_base>(num_draws);
	rlxt::run<swb32_base>(num_draws);
	rlxt::run<swb64_base>(num_draws);
	rlxt::run<swx64_base>(num_draws);

	std::printf("\n");

	rlxt::run<fast_awc16>(num_draws);
	rlxt::run<fast_swb16>(num_draws);
	rlxt::run<fast_awc32>(num_draws);
	rlxt::run<fast_swb32>(num_draws);
	rlxt::run<fast_swb64>(num_draws);

	std::printf("\n");

	rlxt::run<awc16>(num_draws);
	rlxt::run<swb16>(num_draws);
	rlxt::run<awc32>(num_draws);
	rlxt::run<swb32>(num_draws);
	rlxt::run<swb64>(num_draws);
}
