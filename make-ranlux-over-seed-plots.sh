#!/bin/sh

# Copyright 2019 Christoph Conrads
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

set -e

c++ -Wextra -Wall -std=c++11 -pedantic \
	ranlux-over-seed-lcg.cpp -o ranlux-over-seed-lcg
c++ -Wextra -Wall -std=c++11 -pedantic \
	-O3 ranlux-over-seed-counter.cpp -o ranlux-over-seed-counter
c++ -Wextra -Wall -std=c++11 -pedantic \
	ranlux-over-seed-lfsr.cpp -o ranlux-over-seed-lfsr


# set the python matplotlib back-end
export MPLBACKEND="cairo"

./ranlux-over-seed-lcg | \
	python3 plot-ranlux-over-seed.py \
		'LCG initialization' \
		ranlux-over-seed-lcg.png
./ranlux-over-seed-counter | \
	python3 plot-ranlux-over-seed.py \
		'Counter initialization, 10^6 values discarded' \
		ranlux-over-seed-counter.png
./ranlux-over-seed-lfsr | \
	python3 plot-ranlux-over-seed.py \
		'LFSR initialization' \
		ranlux-over-seed-lfsr.png
